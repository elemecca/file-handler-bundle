<?php

namespace BetaMFD\FileHandlerBundle\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use BetaMFD\FileHandlerBundle\Model\FileInterface;
use Symfony\Component\HttpFoundation\File\File as SymfonyFile;

/**
 * This service tries to make file handling as easy as possible
 *
 * Fast usage from form -
 *
 * Handle files straight from the form using
 *     $service->handleForm($form);
 *
 * Allow certain extensions
 *     $service->allowPdf();
 *
 * Verify the extension of uploaded file is okay
 *     $service->testFileType();
 *
 * Test to see if file name is okay (length and checks for duplicates)
 *     $service->testName($new_name, $optional_location);
 *
 * Get File entity (flushed)
 *     $entity = $service->newFile($location, $optional_new_name);
 */
class FileHandler
{
    /** @var \BetaMFD\FileHandlerBundle\Model\UserInterface */
    private $user;

    private $path;

    private $em;

    private $fs;

    private $logger;

    /**
     * Are duplicate hashes an error?
     * If true - will not check for duplicate file hashes
     * If false - will record an error for duplicate file hashes
     *
     * @var boolean
     */
    private $allowDuplicateHashes = false;

    /**
     * allowedMimeTypes
     * whitelist of MIME types that the upload can be
     *
     * @var array
     */
    private $allowedMimeTypes = [];

    /**
     * allowedExtensions
     * if the MIME types fail, these extensions are still okay
     *
     * This is meant as a failsafe for when the MIME types are weird
     * As the MIME array list gets larger, this list should shrink
     *
     * @var array
     */
    private $allowedExtensions = [];

    /**
     * extensionOkay
     * @var boolean
     */
    private $extensionOkay = false;

    /**
     * filenameTaken
     * @var boolean
     */
    private $filenameTaken = false;

    /**
     * uploadedFile
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    private $uploadedFile;

    /**
     * fileEntity
     * @var \BetaMFD\FileHandlerBundle\Model\FileInterface
     */
    private $fileEntity;

    /**
     * errors
     * @var array
     */
    private $errors = [];

    private $hasDuplicate = false;

    private $file_entity_class;

    private $privatePath;

    private $publicPath;

    private $setEntityManually = false;


    public function __construct(
        $private_upload_location,
        $public_upload_location,
        $file_entity,
        \Doctrine\ORM\EntityManagerInterface $em,
        \Symfony\Bridge\Monolog\Logger $logger,
        \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $token_storage
    ) {
        $this->path = $private_upload_location;
        $this->privatePath = $private_upload_location;
        $this->publicPath = $public_upload_location;
        $this->em = $em;
        $this->logger = $logger;
        $this->file_entity_class = $file_entity;
        if ($token_storage->getToken()) {
            $this->user = $token_storage->getToken()->getUser();
        } else {
            $this->user = null;
        }
        $this->fs = new Filesystem();
    }

    private function getRep($repo)
    {
        return $this->em->getRepository($this->file_entity_class);
    }

    private function createFile()
    {
        if ($this->setEntityManually and !empty($this->fileEntity)) {
            $this->fileEntity->setPrivatePath($this->isPrivatePath());
            return $this->fileEntity;
        }
        $class = new \ReflectionClass($this->file_entity_class);
        $entity = $class->newInstance();
        $entity->setPrivatePath($this->isPrivatePath());
        return $entity;
    }

    /**
     * Use this if you've created the file entity before uploading the file
     */
    public function setFileEntity(\BetaMFD\FileHandlerBundle\Model\FileInterface $fileEntity)
    {
        $this->fileEntity = $fileEntity;
        $this->setEntityManually = true;

        return $this;
    }

    public function usePublicPath()
    {
        $this->path = $this->publicPath;
    }

    public function usePrivatePath()
    {
        $this->path = $this->privatePath;
    }

    private function isPrivatePath()
    {
        return $this->path === $this->privatePath;
    }

    /**
     * Allow all reasonable file types
     */
    public function allowAll():void
    {
        $this->allowCsv();
        $this->allowExcel();
        $this->allowWordDoc();
        $this->allowImage();
        $this->allowPdf();
        $this->allowedExtensions[] = 'eml';
    }

    /**
     * Set the extensions and mime types so that csv files should upload fine
     */
    public function allowCsv():void
    {
        $this->allowedExtensions[] = 'csv';
        $this->allowedExtensions[] = 'txt';
        $this->allowedMimeTypes[] = 'text/plain';
    }

    /**
     * Set the extensions and mime types so that excel files should upload fine
     */
    public function allowExcel():void
    {
        $this->allowedExtensions[] = 'xls';
        $this->allowedMimeTypes[] = 'application/vnd.ms-excel';
        $this->allowedExtensions[] = 'xlsx';
        $this->allowedMimeTypes[] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    }

    /**
     * Set the extensions and mime types so that word doc files should upload fine
     */
    public function allowWordDoc():void
    {
        $this->allowedMimeTypes[] = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'; //word doc
        $this->allowedExtensions[] = 'doc';
        $this->allowedExtensions[] = 'docx';
        $this->allowedExtensions[] = 'zip'; //used for word docs generated by PHP
    }

    /**
     * Set the extensions and mime types so that image files should upload fine
     */
    public function allowImage():void
    {
        $this->allowedMimeTypes[] = 'image/png';
        $this->allowedMimeTypes[] = 'image/gif';
        $this->allowedMimeTypes[] = 'image/jpg';
        $this->allowedMimeTypes[] = 'image/jpeg';
        $this->allowedExtensions[] = 'gif';
        $this->allowedExtensions[] = 'jpg';
        $this->allowedExtensions[] = 'jpeg';
        $this->allowedExtensions[] = 'png';
    }

    /**
     * Set the extensions and mime types so that pdf files should upload fine
     */
    public function allowPdf():void
    {
        //$this->allowedExtensions[] = 'pdf'; //this is handled in the allowed mime section
        $this->allowedMimeTypes[] = 'application/pdf';
    }

    /**
     * Allow whatever mime type you want
     * @param string $mimeType
     */
    public function addAllowedMimeType($mimeType)
    {
        $this->allowedMimeTypes[] = $mimeType;
    }

    /**
     * Allow whatever extension you want
     * @param string $extension
     */
    public function addAllowedExtension($extension)
    {
        $this->allowedExtensions[] = $extension;
    }

    /**
     * Given a Symfony Form, starts the process for uploading the file
     *
     * @param \Symfony\Component\Form\Form $form
     * @param string  $field if the field isn't named "file" then what is it named?
     *
     * @return self
     */
    public function handleForm(
        \Symfony\Component\Form\Form $form,
        $field = 'file'
    ) {
        $file = $form->get($field)->getData();

        if (!$file->isValid()) {
            throw new \Exception('File was not uploaded successfully.'
                . $file->getError());
        }

        $this->uploadedFile = $file;

        //check mime type and extension
        //saves and logs errors but does not throw any exceptions
        if (!$this->testMimeType()) {
            $this->testExtension();
        }
        return $this;
    }

    /**
     * Set the value of uploadedFile
     * This is used if this service is not directly handling the form
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile uploadedFile
     *
     * @return self
     */
    public function setUploadedFile(\Symfony\Component\HttpFoundation\File\UploadedFile $uploadedFile)
    {
        $this->uploadedFile = $uploadedFile;

        return $this;
    }

    /**
     * Test the Mime type and then the extension if the mime type isn't explicitly allowed
     * @return boolean
     */
    public function testFileType()
    {
        if ($this->testMimeType()) {
            return true;
        } else {
            return $this->testExtension();
        }
    }

    /**
     * Tests the uploaded file's MIME type against the white list
     * @return boolean
     */
    public function testMimeType()
    {
        $mime = $this->uploadedFile->getMimeType();
        if (array_search($mime, $this->allowedMimeTypes) !== false) {
            $this->extensionOkay = true;
            return true;
        } else {
            $this->logError("Unapproved file uploaded, MIME type $mime not in whitelist.",
                $this->uploadedFile->getClientOriginalName(),
                [
                    'extension' => $this->uploadedFile->guessExtension(),
                    'mime_type' => $this->uploadedFile->getMimeType(),
                    'whitelist' => $this->allowedMimeTypes,
                ]
            );
            return false;
        }
    }

    /**
     * Tests the uploaded file's extension against the white list
     * @return boolean
     */
    public function testExtension()
    {
        $ext = $this->uploadedFile->guessExtension();
        if (array_search($ext, $this->allowedExtensions) !== false) {
            $this->extensionOkay = true;
            return true;
        } else {
            $this->logError("Unapproved file uploaded, extension $ext not in whitelist.",
                $this->uploadedFile->getClientOriginalName(),
                [
                    'extension' => $this->uploadedFile->guessExtension(),
                    'mime_type' => $this->uploadedFile->getMimeType(),
                    'whitelist' => $this->allowedExtensions,
                ]
            );
            return false;
        }
    }

    /**
     * Given a file name, does it already exist? Is it a valid name?
     *
     * @param  string $filename
     * @param  string $subfolder
     * @return boolean
     */
    public function testName($filename, $subfolder = null)
    {
        //the maximum length of the name of a file plus its extension should be less than 255 characters
        if (strlen($filename) > 255) {
            $this->logError('File name is too long', $filename);
            return false;
        }
        //look for filename in database
        //does it exist
        $files = $this->getRep('File')->findByFilename($filename, $subfolder);
        if (!empty($files)) {
            $this->logError("File $filename already exists.", $filename, ['subfolder' => $subfolder]);
            return false;
        }

        //no issues found
        return true;
    }

    /**
     * [fixFilename description]
     * @param  string   $filename
     * @param  string   $replace_spaces set this to whatever you want spaces replaced with
     *                      Can set to '' to remove spaces
     *                      Defaults to false for no changes
     * @return string   new file name
     */
    public function fixFilename($filename, $replace_spaces = false)
    {
        if ($replace_spaces !== false) {
            $filename = str_replace(' ', $replace_spaces, $filename);
        }

        $filename = preg_replace('/[^A-Za-z0-9\- _().$]/', '', $filename);
        return $filename;
    }

    private function logError($errorText, $filename, $otherLog)
    {
        $this->errors[] = $errorText;
        //log the extension error so I can see what happened
        $this->logger->error($errorText, [
            'user' => $this->user ? $this->user->getName() : null,
            'file_name' => $filename,
        ] + $otherLog);
    }

    /**
     * Given a file and a desired new name,
     * take the original file extension and add it onto the new filename
     * @param  SymfonyFile $file
     * @param  string      $newNameWithoutExtension
     * @return string      $newNameWithOldExtension
     */
    public function makeNewFilenameKeepExtension(
        SymfonyFile $file,
        $newNameWithoutExtension
    ){
        $name = $file->getClientOriginalName();
        $path_parts = pathinfo($name);
        $ext = strtolower($path_parts['extension']);
        return $this->fixFilename($newNameWithoutExtension . '.' . $ext);
    }

    /**
     * Given a portal file and a new name, rename that file
     * @param  \BetaMFD\FileHandlerBundle\Model\FileInterface $file
     * @param  string                    $newName
     * @return boolean  Did the file rename okay?
     */
    public function renameFile(
        \BetaMFD\FileHandlerBundle\Model\FileInterface $file,
        $newName = null,
        $newLocation = null,
        $flush = true
    ) {
        //is file deleted?
        if ($file->isDeleted()) {
            $this->logError('File was deleted', $file->getFilename());
            return false;
        }

        $oldname = $file->getFilename();
        $oldLocation = $file->getLocation();
        if (empty($newName)) {
            $newName = $file->getFilename();
        }
        if (empty($newLocation)) {
            $newLocation = $file->getLocation();
        }

        //is the new name valid?
        if (!$this->testName($newName, $newLocation)) {
            //If name/location combo isn't okay
            //errors are logged in testName()
            return false;
        }
        $name = $this->fixFilename($newName);

        //rename the file
        $oldPath = $this->path . $oldLocation . $file->getFilename();
        $newPath = $this->path . $newLocation . $name;
        $this->fs->rename($oldPath, $newPath);

        //rename the file in the Entity
        $file->setFilename($name);
        $file->setLocation($newLocation);

        //flush
        if ($flush) {
            $this->em->flush();
        }

        //no issues found
        return true;
    }

    /**
     * Deletes the file from the harddrive and removes the file entity
     * @param BetaMFD\FileHandlerBundle\Model\FileInterface $file
     */
    public function deleteAndRemoveFile(\BetaMFD\FileHandlerBundle\Model\FileInterface $file)
    {
        $name = $file->getFilename();
        $location = $file->getLocation();
        $this->fs->remove($this->path . $location . $name);

        $this->em->remove($file);
        $this->em->flush();
    }

    /**
     * Deletes the file from the harddrive and updates the file entity
     * @param BetaMFD\FileHandlerBundle\Model\FileInterface $file
     * @param BetaMFD\FileHandlerBundle\Model\UserInterface $deletedBy
     */
    public function deleteFile(
        \BetaMFD\FileHandlerBundle\Model\FileInterface $file,
        \BetaMFD\FileHandlerBundle\Model\UserInterface $deletedBy = null
    ) {
        $name = $file->getFilename();
        $location = $file->getLocation();
        $this->fs->remove($this->path . $location . $name);

        $file->setDeleteDate(new \DateTime);

        if (!empty($deletedBy)) {
            $file->setDeleteBy($deletedBy);
        }

        $this->em->flush();
    }

    /**
     * Run after handleForm()
     * Give this an uploaded file and it will create a new File for you
     * Tries to flush file to database
     * Saves file to a "failed" folder if it can't be flushed to the database
     *
     * @param string $subfolder subfolder location of where to put file
     * @param string $new_name  optional new filename, complete with extension
     * @param boolean $flush     whether or not you want to audo-flush the file (default true)
     *
     * @return File|false
     */
    public function newFile($subfolder, $new_name = null, $flush = true)
    {
        if (empty($this->uploadedFile)) {
            throw new \Exception('No file was saved. Did you handleForm()?');
        }

        //if there were errors, prevent creation of the file!
        if (!empty($this->errors)) {
            return false;
        }

        $file = $this->createFile();
        $this->fileEntity = $file;

        $uploadedFile = $this->uploadedFile;
        $filename = $uploadedFile->getClientOriginalName();
        $file->setOriginalFilename($filename);
        $file->setContentType($this->uploadedFile->getMimeType());

        //get size
        $size = $uploadedFile->getSize(); //in bytes
        $file->setFileSizeB($size);

        return $this->handleNewFile($file, $uploadedFile, $subfolder, $filename, $new_name, $flush);
    }

    /**
     * Run after handleForm()
     * Create a new File for you from an handled form
     * Will check the database to see if there are duplicates and fail
     * Tries to flush file to database
     * Saves file to a "failed" folder if it can't be flushed to the database
     *
     * @param string $subfolder subfolder location of where to put file
     * @param string $new_name  optional new filename, complete with extension
     * @param boolean $flush     whether or not you want to audo-flush the file (default true)
     *
     * @return File|false
     */
    public function newFileNoDuplicates($subfolder, $new_name = null, $flush = true)
    {
        if (empty($this->uploadedFile)) {
            throw new \Exception('No file was saved. Did you handleForm()?');
        }

        //hash file to see if a duplicate exists
        if ($this->hasDuplicate(self::createHash($this->uploadedFile->getPathname()))) {
            $this->errors[] = 'A duplicate file already exists on our server.'
             . ' Please check the file. Please email us if you think this message is in error.';
             return false;
        }
        return $this->newFile($subfolder, $new_name, $flush);
    }

    /**
     * Create a new file entity given a filename and a subfolder
     * This is ideal for files that are code-generated and not uploaded
     * Tries to flush file to database
     * Saves file to a "failed" folder if it can't be flushed to the database
     *
     * @param string  $filename
     * @param string  $subfolder subfolder location of where to put file
     * @param string  $new_name  optional new filename, complete with extension
     * @param boolean $flush     whether or not you want to audo-flush the file (default true)
     *
     * @return File|false
     */
    public function newManualFile($filename, $subfolder, $new_name = null, $flush = true)
    {
        //if there were errors, prevent creation of the file!
        if (!empty($this->errors)) {
            return false;
        }

        $file = $this->createFile();
        $this->fileEntity = $file;
        $file->setOriginalFilename($filename);

        $fullPath = $this->path . $subfolder . '/' . $filename;
        $symfonyFile = new SymfonyFile($fullPath);

        //get size
        $file->setFileSizeB($symfonyFile->getSize());

        //handle the file entity - sets some information and tries to save to database
        return $this->handleNewFile($file, $symfonyFile, $subfolder, $filename, $new_name, $flush);
    }

    /**
     * Handles the new file, whether uploaded or generated
     * Used in newFile() and newManualFile()
     *
     * @param  File        $file        The File entity
     * @param  SymfonyFile $fileHandler Either the uploaded file, or a new SymfonyFile object
     * @param  string      $subfolder   subfolder location of where to put file
     * @param  string      $filename    original filename of file
     * @param  string      $new_name    new name to set file to
     * @param  boolean     $flush       whether or not to flush to database
     *
     * @return File|false
     */
    private function handleNewFile(
        $file,
        SymfonyFile $fileHandler,
        $subfolder,
        $filename,
        $new_name,
        $flush
    ) {
        $file->setUploadBy($this->user);
        if (substr($subfolder, -1) == '\\' or substr($subfolder, -1) == '/') {
            //if the location ends in \ or /, don't add an end!
            $file->setLocation($subfolder);
        } elseif (strpos($subfolder, '\\') !== false) {
            //if the location includes a backslash anywhere, end it with a backslash
            $file->setLocation($subfolder . '\\');
        } else {
            //everything else, end it with a forward slash
            $file->setLocation($subfolder . '/');
        }

        $file->setExtension($fileHandler->guessExtension());

        //use Mime component to check Mime type
        $file->setContentType($fileHandler->getMimeType());

        //hash file
        $file->setHash(self::createHash($fileHandler->getPathname()));
        //test for duplicates (no errors by default)
        if ($this->hasDuplicate($file->getHash())) {
            $this->hasDuplicate = true;
        }

        //move file
        $new_name = empty($new_name) ? $filename : $new_name;
        //remove special characters from new filename.
        $new_name = $this->fixFilename($new_name);
        $file->setFilename($new_name);
        $fileHandler->move($this->path . $subfolder, $new_name);

        if ($file->isImage()) {
            $info = getimagesize($this->path . $subfolder . '/' . $file->getFilename());
            list($x, $y) = $info;
            $file->setWidth($x);
            $file->setHeight($y);
        }

        $this->em->persist($file);

        if (!$flush) {
            return $file;
        } else {
            return $this->tryFlush();
        }
    }

    /**
     * After the file is processed and ready to go,
     *   try to flush it to the database
     *
     * Catches \Doctrine\DBAL\DBALException and saves error, log, and returns false
     *
     * @return File|false
     */
    public function tryFlush() {
        try {
            $this->em->flush();
            return $this->fileEntity;
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->errors[] = "File did not complete upload process. Possible database error.";
            $filename = $this->fileEntity->getFilename();
            $subfolder = $this->fileEntity->getLocation();
            //log the extension error so I can see what happened
            $this->logger->error($e->getMessage(), [
                'user' => $this->user ? $this->user->getName() : null,
                'file_name' => $filename,
                'subfolder' => $subfolder,
            ]);
            //quarantine for review
            $oldpath = $this->path . $subfolder . '/' . $filename;
            $newpath = $this->path . 'failed' . '/' . date('Y-m-d_H.i.s_') . $filename;
            $this->fs->rename($oldpath, $newpath);
            return false;
        }
    }

    /**
     * Create hash for file using hash_file('sha256', $file)
     *
     * @var string $path full path of file
     *
     * @return string $hash
     */
    static public function createHash($path)
    {
        return hash_file('sha256', $path);
    }

    /**
     * Search the database to see if there's already a file with this hash
     *
     * @param  string  $hash
     *
     * @return boolean
     */
    public function hasDuplicate($hash)
    {
        if ($this->allowDuplicateHashes) {
            //if we're allowing duplicate hashes there's no need to check
            return false;
        }
        if (empty($hash)) {
            //don't search for empty strings!
            return;
        }
        $files = $this->getRep('File')->findByHash($hash);
        return !empty($files);
    }

    /**
     * Sets the "Allow Duplicate Hashes" flag to true
     */
    public function allowDuplicateHashes():void
    {
        $this->allowDuplicateHashes = true;
    }

    /**
     * Sets the "Allow Duplicate Hashes" flag to false (default)
     */
    public function dontAllowDuplicateHashes():void
    {
        $this->allowDuplicateHashes = false;
    }

    /**
     * Get the value of allowedMimeTypes
     *
     * @return array
     */
    public function getallowedMimeTypes()
    {
        return $this->allowedMimeTypes;
    }

    /**
     * Set the value of allowedMimeTypes
     *
     * @param array allowedMimeTypes
     *
     * @return self
     */
    public function setallowedMimeTypes(array $allowedMimeTypes)
    {
        $this->allowedMimeTypes = $allowedMimeTypes;

        return $this;
    }

    /**
     * Get the value of allowedExtensions
     *
     * @return array
     */
    public function getAllowedExtensions()
    {
        return $this->allowedExtensions;
    }

    /**
     * Set the value of allowedExtensions
     *
     * @param array allowedExtensions
     *
     * @return self
     */
    public function setAllowedExtensions(array $allowedExtensions)
    {
        $this->allowedExtensions = $allowedExtensions;

        return $this;
    }

    /**
     * Get the value of Errors
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Get the value of Extension Okay
     *
     * @return boolean
     */
    public function getExtensionOkay()
    {
        return $this->extensionOkay;
    }

    /**
     * Get the value of Extension Okay
     *
     * @return boolean
     */
    public function isExtensionOkay()
    {
        return $this->extensionOkay;
    }

    /**
     * getUnspacedFileName
     * replaces spaces with _
     * @return string filename
     */
    public function getUnspacedFileName()
    {
        $name = $this->uploadedFile->getClientOriginalName();
        return str_replace(' ', '_', $name);
    }

    /**
     * Get the value of uploadedFile
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getUploadedFile()
    {
        return $this->uploadedFile;
    }

    public function getUploadedFileName()
    {
        if (empty($this->uploadedFile)) {
            throw new \Exception("No file has been uploaded yet so you can't get its name.");
        }
        return $this->uploadedFile->getClientOriginalName();
    }

    public function getOriginalExtension()
    {
        $originalName = $this->getUploadedFileName();
        return strrchr($originalName, '.');
    }

    public function getUploadedFileNameSansExtension()
    {
        $originalName = $this->getUploadedFileName();
        $ext = $this->getOriginalExtension();

        //https://stackoverflow.com/a/3835653/6078296
        $pos = strrpos($originalName, $ext);
        if($pos !== false)
        {
            $originalName = substr_replace($originalName, '', $pos, strlen($ext));
        }
        return $originalName;
    }

    /**
     * Get the value of filenameTaken
     *
     * @return boolean
     */
    public function getFilenameTaken()
    {
        return $this->filenameTaken;
    }

    /**
     * Get the value of filenameTaken
     *
     * @return boolean
     */
    public function isFilenameTaken()
    {
        return $this->filenameTaken;
    }

    /**
     * Get the value of Has Duplicate
     *
     * @return mixed
     */
    public function getHasDuplicate()
    {
        return $this->hasDuplicate;
    }

    /**
     * Get the value of Path
     *
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Get the value of Private Path
     *
     * @return mixed
     */
    public function getPrivatePath()
    {
        return $this->privatePath;
    }

    /**
     * Get the value of Public Path
     *
     * @return mixed
     */
    public function getPublicPath()
    {
        return $this->publicPath;
    }
}
