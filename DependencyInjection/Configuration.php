<?php

namespace BetaMFD\FileHandlerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        //replace with commented stuff when I no longer need to support symfony 3
        //$treeBuilder = new TreeBuilder('beta_mfd_file_handler');

        $rootNode = $treeBuilder->root('beta_mfd_file_handler');
        $rootNode
        //$treeBuilder->getRootNode()
            ->children()
                ->scalarNode('private_upload_location')
                ->defaultValue('%kernel.project_dir%/var/uploads/')
            ->end()
                ->scalarNode('public_upload_location')
                ->defaultValue('%kernel.project_dir%/web/uploads/')
            ->end()
                ->scalarNode('file_entity')
                ->defaultValue('App/Entity/File')
            ->end()
        ;

        return $treeBuilder;
    }
}
