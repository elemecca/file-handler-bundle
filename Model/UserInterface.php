<?php

namespace BetaMFD\FileHandlerBundle\Model;

interface UserInterface
{
    public function getName();
}
